import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeMap;

public class Main {

    public static void main(String args[]) {
        String accountNumber;
        String accountType;
        String givenName;
        String familyName;

        String[] account = new String[4];
        ArrayList<String> accounts = new ArrayList<>();

        TreeMap<String, Account> map = new TreeMap<String, Account>();

        File file = new File("C:/Users/ethan/Documents/Winter 2018/CIT 360/rawData/rawData.csv");
        BufferedReader br = null;
        String line;
        String separator = ",";

        try {
            br = new BufferedReader(new FileReader(file));

            while ((line = br.readLine()) != null) {
                account = line.split(separator);
                accountNumber = account[0];
                accountType = account[1];
                givenName = account[2];
                familyName = account[3];
                Account theAccount = new Account(accountNumber, accountType, givenName, familyName);
                map.put(accountNumber, theAccount);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Number of accounts: " + map.size());

        Set<String> keys = map.keySet();
        for (String key: keys) {
            Account theAccount = map.get(key);
            System.out.println();
            System.out.println("Account Number: " + theAccount.getAccountNumber());
            System.out.println("Account Type: " + theAccount.getAccountType());
            System.out.println("Given Name: " + theAccount.getGivenName());
            System.out.println("Family Name: " + theAccount.getFamilyName());
            System.out.println();
        }



    }

}
