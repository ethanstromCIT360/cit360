import org.junit.Assert;
import org.junit.Test;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MammalBeanTest {
    private MammalBean mammal1 = new MammalBean(4, "Black", 5);
    private MammalBean mammal2 = new MammalBean(3, "Purple", 4);
    private MammalBean mammal3 = new MammalBean(4, "Red", 3);
    private MammalBean mammal4 = new MammalBean(3, "Blue", 2);
    private MammalBean mammal5 = new MammalBean(4, "Orange", 1);

    private DogBean dog1 = new DogBean(10, "Yellow", 20, "Mutation1", "Dog One");
    private DogBean dog2 = new DogBean(9, "Cyan", 21, "Mutation2", "Dog Two");
    private DogBean dog3 = new DogBean(8, "Indigo", 22, "Mutation3", "Dog Three");
    private DogBean dog4 = new DogBean(7, "White", 23, "Mutation4", "Dog Four");
    private DogBean dog5 = new DogBean(6, "Brown", 24, "Mutation5", "Dog Five");

    @Test
    public void getLegCount() {
        Assert.assertEquals(4, mammal1.getLegCount());
        Assert.assertEquals(3, mammal2.getLegCount());
        Assert.assertEquals(4, mammal3.getLegCount());
        Assert.assertEquals(3, mammal4.getLegCount());
        Assert.assertEquals(4, mammal5.getLegCount());

        Assert.assertEquals(10, dog1.getLegCount());
        Assert.assertEquals(9, dog2.getLegCount());
        Assert.assertEquals(8, dog3.getLegCount());
        Assert.assertEquals(7, dog4.getLegCount());
        Assert.assertEquals(6, dog5.getLegCount());
    }

    @Test
    public void getColor() {
        Assert.assertEquals("Black", mammal1.getColor());
        Assert.assertEquals("Purple", mammal2.getColor());
        Assert.assertEquals("Red", mammal3.getColor());
        Assert.assertEquals("Blue", mammal4.getColor());
        Assert.assertEquals("Orange", mammal5.getColor());

        Assert.assertEquals("Yellow", dog1.getColor());
        Assert.assertEquals("Cyan", dog2.getColor());
        Assert.assertEquals("Indigo", dog3.getColor());
        Assert.assertEquals("White", dog4.getColor());
        Assert.assertEquals("Brown", dog5.getColor());
    }

    @Test
    public void getHeight() {
        Assert.assertEquals(5, mammal1.getHeight(), 0);
        Assert.assertEquals(4, mammal2.getHeight(), 0);
        Assert.assertEquals(3, mammal3.getHeight(), 0);
        Assert.assertEquals(2, mammal4.getHeight(), 0);
        Assert.assertEquals(1, mammal5.getHeight(), 0);

        Assert.assertEquals(20, dog1.getHeight(), 0);
        Assert.assertEquals(21, dog2.getHeight(), 0);
        Assert.assertEquals(22, dog3.getHeight(), 0);
        Assert.assertEquals(23, dog4.getHeight(), 0);
        Assert.assertEquals(24, dog5.getHeight(), 0);

    }

    @Test
    public void getBreed() {
        Assert.assertEquals("Mutation1", dog1.getBreed());
        Assert.assertEquals("Mutation2", dog2.getBreed());
        Assert.assertEquals("Mutation3", dog3.getBreed());
        Assert.assertEquals("Mutation4", dog4.getBreed());
        Assert.assertEquals("Mutation5", dog5.getBreed());
    }

    @Test
    public void getName() {
        Assert.assertEquals("Dog One", dog1.getName());
        Assert.assertEquals("Dog Two", dog2.getName());
        Assert.assertEquals("Dog Three", dog3.getName());
        Assert.assertEquals("Dog Four", dog4.getName());
        Assert.assertEquals("Dog Five", dog5.getName());
    }

    Set<MammalBean> mammalSet = new HashSet<MammalBean>();

    public void addToSet() throws Exception {
        mammalSet.add(mammal1);
        mammalSet.add(mammal2);
        mammalSet.add(mammal3);
        mammalSet.add(mammal4);
        mammalSet.add(mammal5);
    }

    @Test
    public void testSetBeforeRemove() {
        try {
            addToSet();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertSame(true, mammalSet.contains(mammal1));
        Assert.assertSame(true, mammalSet.contains(mammal2));
        Assert.assertSame(true, mammalSet.contains(mammal3));
        Assert.assertSame(true, mammalSet.contains(mammal4));
        Assert.assertSame(true, mammalSet.contains(mammal5));
    }

    public void removeFromSet() throws Exception {
        mammalSet.remove(mammal2);
        mammalSet.remove(mammal4);
    }

    @Test
    public void testSetAfterRemove() {
        try {
            addToSet();
            removeFromSet();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertSame(true, mammalSet.contains(mammal1));
        Assert.assertSame(false, mammalSet.contains(mammal2));
        Assert.assertSame(true, mammalSet.contains(mammal3));
        Assert.assertSame(false, mammalSet.contains(mammal4));
        Assert.assertSame(true, mammalSet.contains(mammal5));
    }

    Map<String, DogBean> dogMap = new HashMap<String, DogBean>();
    String name1 = dog1.getName();
    String name2 = dog2.getName();
    String name3 = dog3.getName();
    String name4 = dog4.getName();
    String name5 = dog5.getName();

    public void putToMap() throws Exception {
        dogMap.put(name1, dog1);
        dogMap.put(name2, dog2);
        dogMap.put(name3, dog3);
        dogMap.put(name4, dog4);
        dogMap.put(name5, dog5);
    }

    @Test
    public void testMapBeforeRemove() {
        try {
            putToMap();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertSame(true, dogMap.containsValue(dog1));
        Assert.assertSame(true, dogMap.containsValue(dog2));
        Assert.assertSame(true, dogMap.containsValue(dog3));
        Assert.assertSame(true, dogMap.containsValue(dog4));
        Assert.assertSame(true, dogMap.containsValue(dog5));
    }

    public void removeFromMap() throws Exception {
        dogMap.remove(name1, dog1);
        dogMap.remove(name3, dog3);
    }

    @Test
    public void testMapAfterRemove() {
        try {
            putToMap();
            removeFromMap();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertSame(false, dogMap.containsValue(dog1));
        Assert.assertSame(true, dogMap.containsValue(dog2));
        Assert.assertSame(false, dogMap.containsValue(dog3));
        Assert.assertSame(true, dogMap.containsValue(dog4));
        Assert.assertSame(true, dogMap.containsValue(dog5));
    }
}