import java.util.Arrays;

public class Kennel {

    DogBean[] buildDogs() {

        DogBean dog1 = new DogBean(4, "Black", 1, "Poodle", "Dog One");
        DogBean dog2 = new DogBean(4, "Purple", 1, "German Shepard", "Dog Two");
        DogBean dog3 = new DogBean(4, "Red", 1, "Golden Retreiver", "Dog Three");
        DogBean dog4 = new DogBean(4, "Blue", 1, "Lab", "Dog Four");
        DogBean dog5 = new DogBean(4, "Orange", 1, "Schnauzer", "Dog Five");

        DogBean[] dogs = {dog1, dog2, dog3, dog4, dog5};

        return dogs;
    }

    public void displayDogs(DogBean[] dogs) {
        for (int i = 0; i < dogs.length; i++) {
            System.out.print(dogs[i]);
        }
    }

    public static void main(String args[]) {
        Kennel kennel = new Kennel();
        DogBean[] allDogs = kennel.buildDogs();
        kennel.displayDogs(allDogs);
    }
}
