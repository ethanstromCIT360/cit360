public class DogBean extends MammalBean {

    private String breed;
    private String name;

    //Constructors
    public DogBean(int legCount,String color, double height, String breed, String name) {
        super(legCount, color, height);
        this.breed = breed;
        this.name = name;
    }

    //Getters and Setters
    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return super.toString() + "\n" +
                "breed=" + this.breed + "\n" +
                "name=" + this.name + "\n" +
                "legCount=" + super.getLegCount() + "\n" +
                "color=" + super.getColor() + "\n" +
                "height=" + super.getHeight() + "\n\n";
    }

}


